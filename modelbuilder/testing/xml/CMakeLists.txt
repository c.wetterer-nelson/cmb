# This is a list of test XML files.
# When you add an entry **be sure to describe the actions a user
# must perform to reproduce the test** as it can be painful to
# read the XML when a test needs to be re-recorded.
# **It is also useful to tersely describe what functionality is
# being tested if not clear from the procedural description and
# test name.**
set(xml_tests_with_baselines
  # Just open an SMTK file and ensure it renders properly.
  # Also tests default color-by mode in theory.
  OpenModelFile
  # Open 2 model files; set 1 to color-by-entity, 1 to color-by-volume.
  # Zoom to second model so colors can be discerned.
  PerResourceColorByMode
  # Open an SMTK model, block select some entities, then change the selection filters
  # so only the edges remain selected.
  SelectionFilterJustEdges
  # Open an SMTK model then select 1 model face in the tree view.
  SelectModelEntityInTree
  # Open an SMTK model then select 1 model face in the rendered view via block-selection.
  SelectModelEntityInRendering
  # File .. New Resource .. Oscillator: Create new oscillator with defaults.
  # In Resources, expand new oscillator geometry, choose simulation domain.
  # In Operations, double-click EditSource. Change x position to 0.2,
  # and radius to 0.15. Apply.
  # In Attribute editor, Solver, job size to 1
  # source terms, change source 1 to periodic source, omega to 0.99
  # In Resources, choose new oscillator simulation
  # in Operations, Export
  # filename, type foo.osc, Apply
  OscillatorSingle
)

set(xml_tests_no_baselines
  # Open knee.smtk. Auto-apply changes.
  # x-axis view, rotate to ~45 deg view
  # Close properties panel
  # 4th block, Operations AssignColors
  # editor, choose color, blue - image-compare
  # Resources, ctrl-A to select all (Not recorded),
  # ctrl-click top two Knee, so blocks are selected (modifier not recorded, selects knee)
  # Until multi-select supported: select just 755, the femur.
  # Click replace item's members with selection button.
  # Operation editor, click change palette - brewer 11
  # palette button, then select knee to see the new colors - image-compare
  # optional: switch to kaams palette, - image-compare
  ColorKnee
)

if (ParaView_Version VERSION_GREATER 5.8.1)
  list(APPEND xml_tests_no_baselines
  # Open smtk file, select everything with a drag, then take away faces, edges
  # and vertices, and make sure the selection updates. Multi-image compares inline.
  # finally choose edge select, and select everything in render view.
  ModelSelection
  )
endif()

set(_image_fail_regex "(\n|^)ERROR: The following event FAILED")

set(test_images ${cmb_SOURCE_DIR}/data/baselines/modelbuilder)
if (cmb_data_dir AND cmb_test_data_dir)
  foreach(test ${xml_tests_with_baselines})
    #get_image_threshold_arg(THRESHOLD_CMD ${test})
    set(tname "${test}")
    add_test(
      NAME ${tname}
      COMMAND modelbuilder
        -dr
        --data-directory=${cmb_test_data_dir}
        --test-script=${CMAKE_CURRENT_SOURCE_DIR}/${test}.xml
        --test-directory=${cmb_test_dir}
        --test-baseline=${test_images}/${test}.png
        --exit
    )
    set(tlabels cmb modelbuilder ${${test}-labels})
    set_tests_properties(${tname}
      PROPERTIES
        LABELS "${tlabels}"
    )
    # if a plugins file was created, use it
    if (${plugins_file})
      set_tests_properties(${tname}
        PROPERTIES
          ENVIRONMENT "PV_PLUGIN_CONFIG_FILE=${plugins_file}"
      )
    endif()
  endforeach()
  foreach(test ${xml_tests_no_baselines})
    #get_image_threshold_arg(THRESHOLD_CMD ${test})
    set(tname "${test}")
    add_test(
      NAME ${tname}
      COMMAND modelbuilder
        -dr
        --data-directory=${cmb_test_data_dir}
        --test-script=${CMAKE_CURRENT_SOURCE_DIR}/${test}.xml
        --test-directory=${cmb_test_dir}
        --exit
    )
    set(tlabels cmb modelbuilder ${${test}-labels})
    set(plug_cfg "")
    # if a plugins file was created, use it
    if (${plugins_file})
      set(plug_cfg "PV_PLUGIN_CONFIG_FILE=${plugins_file}")
    endif()
    set_tests_properties(${tname}
      PROPERTIES
        LABELS "${tlabels}"
        FAIL_REGULAR_EXPRESSION "${_image_fail_regex}"
        ENVIRONMENT
          "PARAVIEW_TEST_BASELINE_DIR=${test_images} ${plug_cfg}"
    )
  endforeach()
endif()
