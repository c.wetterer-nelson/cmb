=======================
CMB 21.07 Release Notes
=======================

See also `CMB 21.05 Release Notes`_ for previous changes.

.. _`CMB 21.05 Release Notes`: cmb-21.05.md

OpenCasacade Support
====================
Modelbuilder now comes with support to load IGES, STEP, and OpenCascade
files via OpenCascade_ . Only viewing is currently supported.
In the future, annotations on CAD models as well as modeling operations
will be added.

In addition, OpenCascade SMTK Resources can now be saved and loaded into ModelBuilder.

Finally, distributed binaries will now be built with OpenCascade support.

.. _OpenCascade: https://www.opencascade.com/


Platform Support
================
ARM-Macs are now supported.

SMTK Related Changes
====================
For additional information on changes to SMTK, please see [SMTK-21.07](https://gitlab.kitware.com/cmb/smtk/-/blob/master/doc/release/smtk-21.07.rst)
